# In vivo dissection of a clustered-CTCF domain boundary reveals developmental principles of regulatory insulation

***Chiara Anania, Rafael D. Acemel, Johanna Jedamzick, Adriano Bolondi, Giulia Cova, Norbert Brieske, Ralf Kühn, Lars Wittler, Francisca M. Real, Darío G. Lupiáñez***

![One brick at a time!](notebooks/tads.gif)

bioRxiv 2021.04.14.439779; [Link](https://doi.org/10.1101/2021.04.14.439779)

Here we provide the code supporting the manuscript, mostly in the form of notebooks in Python and R. Below you can find an index relating the different notebooks with the analysis and figures they are relevant for. 

1. [Low coverage filter](notebooks/1-FilterLowCoverage.ipynb). The notebook to filter out poorly covered regions from public Hi-C maps (to exclude artifactual boundaries in such regions from further analysis). 

2. [Relationship between CTCF binding sites and insulation strength at boundaries](notebooks/2-CTCF-insulation.ipynb). Analysis of public Hi-C and ChIP-seq experiments in mouse ES-cells (Bonev et al. 2017) to see how insulation strength scales with the number of CTCF binding sites and their orientation. Figures 1D and 4A rely on these analysis. 

3. Capture-C. 
- [Capture-C analysis](notebooks/3.1-CaptureC.ipynb). Most of the analysis and representation involving the Capture-C experiments. Includes insulation score calculation, loop close-ups and substraction matrices. Here is evident how panels 1A, 1C, 2C, 2D, 3G, 3H, 4D, 4E, 5C and 5D were generated. Also Supp. Fig. 8. 
- [Capture-C derived virtual 4C](notebooks/3.2-CaptureC_Pax3v4c.ipynb). Virtual 4C experiments from the Pax3 promoter derived from the different Capture-C are plotted here using ggplot2. 
- [Capure-C correlations](notebooks/3.3-CaptureC-correlations.ipynb). Correlation analysis from the different Capture-C matrices outside from the target TADs are presented here.

4. [Non-convergent loops](notebooks/4-NC_Loops.ipynb). All the analysis related to the identification and characterization of non-convergent loops in public Hi-C datasets of mouse ES-cells (Bonev et al. 2017). Plots from figures 3B, 3C and Supp. Fig. 9 are derived from these notebook. Complete tables of post-hoc p-values from these analysis are also found here. 
- [Non-convergent loops heatmaps](notebooks/4.1-Heatmaps.ipynb) here the enrichment analysis of Rad21, Maz,conservation and ATAC over convergent and non-convergent loops.  

5. [Non-divergent boundaries examples](notebooks/5.SS-Boundaries_Snaphots.ipynb). Examples of boundaries without divergently oriented CBS used in Supp. Fig. 11.

6. [Convergent/Non-convergent pairs of loops examples](notebooks/6.NC-Loops_Snaphots.ipynb). Examples of pairs of convergent/non-convergent loops (Supp. Fig. 10). 

7. [Range of boundary strengths](notebooks/7-BS_range.ipynb). Boundary score distributions observed in public datasets (Bonev et al. 2017: mouse ES-cells, NPC, CN) analyzed in the context of the boundary scores obtained in the different mutants of the EP boundary (Supp. Fig. 13). 

8. [All Capture-C experiments](notebooks/8-CaptureWholeRegion.ipynb). General view of all the Capture-C experiments presented, the whole ~8.3Mb captured region is displayed for each of the mutants. (Supp. Fig. 7). 

9. [EP Boundary](notebooks/9-Stable_boundary.ipynb). Publicly available Hi-C and ChIP-seq datasets in mouse around the EP-boundary showing that the boundary and the CTCF binding persists across different cell-types (Bonev et al. 2017: ES-cells, CN, NPC; Rodríguez-Carballo et al. 2017: distal-limb, proximal-limb). Used for Supp. Fig. 2.
