inv_start = 77861422 - 71000000 - 1469996
inv_end = 78062382 - 71000000 - 1469996

refGenomeFilename = "../data/genomes/mm9_DelBS.fa"
invGenomeFilename = "../data/genomes/mm9_DelBS_inv.fa"

with open(refGenomeFilename,"r") as ref, open(invGenomeFilename,"w") as inv:
    for record in SeqIO.parse(ref,format="fasta"):
        if record.id == "mm9_Epha4":
            beginning = record.seq[0:inv_start]
            inversion = record.seq[inv_start:inv_end].reverse_complement()
            end = record.seq[inv_end:]
            newSeq = SeqIO.SeqRecord(beginning + inversion + end,id=record.id)
            SeqIO.write(newSeq,inv,"fasta")
        else:
            SeqIO.write(record,inv,"fasta")