from subprocess import Popen,PIPE,call
from sys import stderr,stdout,exit
from argparse import ArgumentParser
import re
from os import path

def iwrite(message):
	stderr.write(message)
	stderr.flush()

def bwa_map(fq1,fq2):
	maplog = open(prefix + ".bwa.log","w")
	bamname = prefix + ".bam"
	bam = open(bamname,"wb")
	bwa_cmd = ["bwa","mem","-t",cpus,"-SP",idx,fq1,fq2]
	samtools_cmd = ["samtools","view","-b"]
	bwa = Popen(bwa_cmd,stdout=PIPE,stderr=maplog)
	samtools = Popen(samtools_cmd,stdin=bwa.stdout,stdout=bam,stderr=maplog)
	samtools.communicate()
	bam.close()
	maplog.close()
	return(bamname)

def bam2pairs(bam): 
	pairs_logname = prefix + ".pairs.log"
	pairs_log = open(pairs_logname,"a")
	pairs_name = prefix + ".pairs"
	pairsfile = open(pairs_name,"w")
	pairs_cmd = ["pairtools","parse","--drop-seq","--drop-sam","-c",fai,bam]#,"-o",pairs_name]
	select_cmd = "pairtools select '(pair_type == \"UU\") or (pair_type == \"UR\") or (pair_type == \"RU\")'"
	pairs = Popen(pairs_cmd,stderr=pairs_log,stdout=PIPE)
	select = Popen(select_cmd,stdin=pairs.stdout,stderr=pairs_log,stdout=pairsfile,shell=True)
	select.communicate()
	pairsfile.close()
	pairs_log.close()
	bgzip = Popen(["bgzip",pairs_name])
	bgzip.communicate()
	pairs_name += ".gz"
	return(pairs_name)

def sortpairs(pairs):
	sort_log = open(prefix + ".sort.log","w")
	sorted_file = prefix + ".sorted.pairs.gz"
	sort_cmd = ["pairtools","sort","--nproc",cpus,"-o",sorted_file,pairs]
	sort = Popen(sort_cmd,stderr=sort_log)
	sort.communicate()
	sort_log.close()
	return(sorted_file)

def dedup(spairs):
	dedup_log = open(prefix + ".dedup.log","w")
	dedup_stats = prefix + ".dedup.stats"
	dedup_file = prefix + ".dedup.pairs.gz"
	dedup_cmd = ["pairtools","dedup","--output-stats",dedup_stats,"-o",dedup_file,spairs]
	dedup = Popen(dedup_cmd,stderr=dedup_log)
	dedup.communicate()
	dedup_log.close()
	return(dedup_file)

def filtering(dedup,digest):
	filtering_log = open(prefix + ".filtering.log","w")
	if not digest:
		digest = "{}_{}.bed".format(path.splitext(path.basename(fasta))[0],renz)
		digest_cmd = ["cooler","digest",fai,fasta,renz,"-o",digest]
		iwrite(" ".join(digest_cmd))
		digestp = Popen(digest_cmd,stderr=filtering_log)
		digestp.communicate()
	dangling_fname = prefix + ".dangling.pairs"
	dangling_file = open(dangling_fname,"w")
	filtered_fname = prefix + ".filtered.pairs"
	filtered_file = open(filtered_fname,"w")
	filt_dict = {"valid":0,"dangling-ends":0,"self-circles":0,"error":0,"extra-dangling":0}
	restrict_cmd = ["pairtools","restrict","-f",digest,dedup]
	restrict = Popen(restrict_cmd,stdout=PIPE,stderr=filtering_log,encoding="utf8")
	tcount = 0
	for line in iter(restrict.stdout.readline,''):
		if line.startswith("#"):
			dangling_file.write(line)
			filtered_file.write(line)
		else:
			tcount +=1
			spline = line.rstrip().split("\t")
			strand1,strand2,frag1,frag2 = spline[5],spline[6],int(spline[8]),int(spline[11])
			diff = abs(frag2-frag1)
			if diff > 1:
				filt_dict["valid"]+=1
				filtered_file.write(line)
			elif diff==1:
				if (strand1=="+") & (strand2=="-"):
					filt_dict["extra-dangling"]+=1
					dangling_file.write(line)
				else:
					filt_dict["valid"]+=1
					filtered_file.write(line)
			else:
				if (strand1=="+") & (strand2=="-"):
					filt_dict["dangling-ends"]+=1
					dangling_file.write(line)
				elif (strand1=="-") & (strand2=="+"):
					filt_dict["self-circles"]+=1
				else:
					filt_dict["error"]+=1
	restrict.communicate()
	filtered_file.close()
	dangling_file.close()
	for cat,count in filt_dict.items():
		towrite = "{}:{} ({}%)\n".format(cat,str(count),str(count*100/tcount))
		filtering_log.write(towrite)
	filtering_log.write("{}:{}\n".format("total",str(tcount)))
	filtering_log.close()
	bgzip1 = Popen(["bgzip",filtered_fname])
	bgzip1.communicate()
	bgzip2 = Popen(["bgzip",dangling_fname])
	bgzip2.communicate()
	filtered_fname += ".gz"
	return(filtered_fname)

def cooler(filtered,cfai,res=5000):
	cooler_log = open(prefix + ".cool.log","w")
	cooler_name = re.sub("filtered.pairs.gz",str(res)+".cool",filtered)
	bins = cfai + ":" + str(res)
	pairix = Popen(["pairix",filtered],stderr=cooler_log)
	pairix.communicate()
	cooler_cmd = ["cooler","cload","pairix","-p",cpus,bins,filtered,cooler_name]
	cool = Popen(cooler_cmd,stderr=cooler_log,stdout=cooler_log)
	cool.communicate()
	zoom = Popen(["cooler","zoomify","--legacy",cooler_name],stderr=cooler_log,stdout=cooler_log)
	zoom.communicate()
	cooler_name = re.sub("cool","mcool",cooler_name)
	return(cooler_name)


parser = ArgumentParser(description="HiC analysis pipeline")
parser.add_argument('-fq1',
					dest="fq1",
					metavar="FQ1",
					type=str,
					help="Fastq file with mate 1")
parser.add_argument('-fq2',
					dest="fq2",
					metavar="FQ2",
					type=str,
					help="Fastq file with mate 2")
parser.add_argument('-bam',
					dest="bamname",
					metavar="BAM",
					type=str,
					help="Bamfile with the already aligned reads using bwa mem")
parser.add_argument('-pairs',
					dest="pairs",
					metavar="PAIRS",
					type=str,
					help="Pairs file already parsed")
parser.add_argument('-dedup',
					dest="dedup",
					metavar="DEDUP",
					type=str,
					help="Pairs file with already deduplicated pairs")
parser.add_argument('-fasta',
					dest="fasta",
					metavar="FASTA",
					type=str,
					help="Fasta file of the reference genome",
					required=True)
parser.add_argument('-fai',
					dest="fai",
					metavar="FAI",
					type=str,
					help="Chromsizes file",
					required=True)
parser.add_argument('-cfai',
					dest="cfai",
					metavar="COOLER FAI",
					type=str,
					help="Chromsizes file for calculating the matrix")
parser.add_argument('-idx',
					dest="idx",
					metavar="BWA INDEX",
					type=str,
					help="Bwa index")
parser.add_argument('-digest',
					dest="digest",
					metavar="DIGEST",
					type=str,
					help="Bedfile with the coordinates of renz fragments",
					required=False)
parser.add_argument('-prefix',
					dest="prefix",
					metavar="PREFIX",
					type=str,
					help="Prefix for output files",
					default="unknown")
parser.add_argument('-renz',
					dest="renz",
					metavar="RESTRICTION ENZYME",
					type=str,
					help="Restriction enzyme of the HiC experiment",
					default="DpnII")
parser.add_argument('-res',
					dest="res",
					metavar="RESOLUION",
					type=int,
					help="Resolution for cooler files",
					default=5000)
parser.add_argument('-p',
					dest="cpus",
					metavar="CPUS",
					type=str,
					help="Number of cores",
					default="1")


args = parser.parse_args()

fq1 = args.fq1
fq2 = args.fq2 
bamname = args.bamname
pairs_name = args.pairs
dedup_name = args.dedup
global idx,fasta,cpus,prefix,fai,renz
idx = args.idx
cpus = args.cpus
prefix = args.prefix
fasta = args.fasta
fai = args.fai
cfai = args.cfai
if not cfai:
	cfai = fai
res = args.res
renz = args.renz 
digest = args.digest

if not ((fq1 and fq2 and idx) or (bamname) or (pairs_name) or (dedup_name)): 
	exit("Either fastqs and bwa idx or bamfiles or pairs file are needed! Exiting! \n")

if not ((bamname) or (pairs_name) or (dedup_name)): 
	iwrite("[HiC_pipe] Mapping with BWA mem (local alignment) ...\n")
	bamname = bwa_map(fq1,fq2)
	iwrite("[HiC_pipe] Finished mapping, check {} file \n".format(bamname))

if not ((pairs_name) or (dedup_name)):
	iwrite("[HiC_pipe] Converting to pairsam ... \n")
	pairs_name = bam2pairs(bamname)
	iwrite("[HiC_pipe] Finished conversion, check {} file \n".format(pairs_name))

if not dedup_name:
	iwrite("[HiC_pipe] Sorting ... \n")
	sorted_name = sortpairs(pairs_name)
	iwrite("[HiC_pipe] Finished sorting, check {} file \n".format(sorted_name))
	iwrite("[HiC_pipe] Deduplicating ... \n")
	dedup_name = dedup(sorted_name)
	iwrite("[HiC_pipe] Finished deduplication, check {} file\n".format(dedup_name))

iwrite("[HiC_pipe] Filtering reads ...\n")
filtered_name = filtering(dedup_name,digest)
iwrite("[HiC_pipe] Filtering done, check {} file\n".format(filtered_name))

iwrite("[HiC_pipe] Calculating cooler file ... \n")
cooler_name = cooler(filtered_name,cfai,res=res)
iwrite("[HiC_pipe] Matrix done, check {} file\n".format(cooler_name))

iwrite("[HiC_pipe] Done!\n")

